export const appConstant = {
    baseURL: 'https://apis.google.com',

    localstorageKeys: {
        token: 'token'
    }
}