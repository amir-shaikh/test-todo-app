import axios from "axios";
import { appConstant } from "../constants/appConstant";

const userApiService = {}

userApiService.login = ({ username, password }) => axios.post(`${appConstant.baseURL}`, { username, password })

export { userApiService };