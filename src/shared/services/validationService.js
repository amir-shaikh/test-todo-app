const validationService = {};

// required field validation
validationService.required = (value) => {
  let isValid = 1;
  let message = '';

  if (
    typeof value === 'object' &&
    value !== null &&
    value !== undefined &&
    !Object.values(value).length
  ) {
    message = 'This field is required';
    isValid = 0;
  }

  if ([null, undefined, ''].includes(value)) {
    isValid = 0;
    message = 'This field is required';
  }

  return { isValid, message };
};

validationService.maxLength = (value, maxLength) => {
  let { isValid, message } = validationService.required(value);

  if (isValid && maxLength && value.length > maxLength) {
    message = `Maximum ${maxLength} characters allowed`;
    isValid = 0;
  }

  return {
    isValid,
    message,
  };
};

validationService.minLength = (value, minLength) => {
  let { isValid, message } = validationService.required(value);

  if (isValid && minLength && value.length <= minLength) {
    message = `Minimum ${minLength} characters allowed`;
    isValid = 0;
  }

  return {
    isValid,
    message,
  };
};

export { validationService };
