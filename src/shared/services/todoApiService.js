import axios from "axios";
import { appConstant } from "../constants/appConstant";

const todoApiService = {};

// API call for create todo
todoApiService.get = () =>
    axios.get(`${appConstant.baseURL}`);

// API call for create todo
todoApiService.create = ({ title, description }) =>
    axios.post(`${appConstant.baseURL}`, { title, description });

// API call for update todo
todoApiService.update = ({ id, title, description }) =>
    axios.put(`${appConstant.baseURL}/${id}`, { title, description });

// API call for delete todo
todoApiService.delete = ({ id }) =>
    axios.delete(`${appConstant.baseURL}/${id}`);

export { todoApiService };
