import { Outlet, Navigate } from "react-router-dom"
import PropTypes from 'prop-types'

const PrivateRoute = (props) => {
    if (!props.isAuthenticated) {
        return <Navigate to="/login" replace />
    }
    return <Outlet />
}

PrivateRoute.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
}

export default PrivateRoute