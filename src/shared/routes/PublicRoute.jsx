import { Navigate, Outlet } from "react-router-dom"
import PropTypes from 'prop-types'

const PublicRoute = (props) => {
    if (props.isAuthenticated) {
        return <Navigate to="/" replace />
    }
    return <Outlet />
}

PublicRoute.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
}

export default PublicRoute