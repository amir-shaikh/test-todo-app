import { configureStore } from '@reduxjs/toolkit'
import { userReducerPath, userReducers } from './slices/userSlice';
import { persistStore } from 'redux-persist';

const store = configureStore({
    reducer: {
        [userReducerPath]: userReducers,
    },
})

export const persistor = persistStore(store)

export default store;