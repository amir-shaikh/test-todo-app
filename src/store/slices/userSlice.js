import { createSlice } from "@reduxjs/toolkit";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const reducerPath = "user"
const initialState = {
    isAuthenticated: false,
    token: null,
};

const userSlice = createSlice({
    name: reducerPath,
    reducerPath,
    initialState,
    reducers: {
        setAuthenticated: (state, payload) => {
            state.isAuthenticated = true;
            state.token = payload.token;
        },
        logout: (state) => {
            state.isAuthenticated = false;
            state.token = null;
        },
    },
});

export const userActions = {
    setAuthenticated: userSlice.actions.setAuthenticated,
    logout: userSlice.actions.logout,
};

export const userReducerPath = userSlice.reducerPath;

// using redux persist package to cache redux data in localstorage
// and keep it in sync
// below code will cache only isAuthenticated and token keys
export const userReducers = persistReducer(
    {
        key: userReducerPath,
        storage,
        whitelist: ["isAuthenticated", "token"],
    },
    userSlice.reducer
);
