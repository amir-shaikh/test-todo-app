import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { Button, Container, Form, FormControl, FormGroup, FormLabel, FormText, Stack, Table } from "react-bootstrap"

import { validationService } from "../shared/services/validationService";
import { todoApiService } from "../shared/services/todoApiService";

const Dashboard = () => {
    const {
        control,
        handleSubmit,
        setError,
        reset,
    } = useForm({
        defaultValues: {
            id: null,
            title: '',
            description: '',
        }
    })

    const [todos, setTodos] = useState([]);

    useEffect(() => {
        setTodos(Array(5).fill({
            id: 1,
            title: 'Todo title',
            description: 'Todo Description'
        }))
    }, [])

    const handleEditClick = (row) => {
        reset({
            id: row.id,
            title: row.title,
            description: row.description
        })
    }

    const handleDeleteClick = async (row) => {
        try {
            const response = await todoApiService.delete({ id: row.id });

            // if user is success then update redux store
            if (response.data) {
                // fetch todos and update list
                const response = await todoApiService.get();
                console.log({ response })
            }
        } catch (error) {
            alert("Delete api failed")
            console.log({ error })
        }
    }

    const columns = [{
        label: 'Sr. No.',
        renderer: (row, rowIndex) => {
            return <span>{rowIndex + 1}</span>
        }
    }, {
        label: 'Title',
        renderer: (row) => {
            return <span>{row.title}</span>
        }
    }, {
        label: 'Description',
        renderer: (row) => {
            return <span>{row.description}</span>
        }
    }, {
        label: 'Action',
        renderer: (row, rowIndex) => {
            return (
                <Stack direction="horizontal" gap={2}>
                    <Button variant="primary" onClick={() => handleEditClick(row, rowIndex)}>Edit</Button>
                    <Button variant="danger" onClick={() => handleDeleteClick(row, rowIndex)}>Delete</Button>
                </Stack>
            )
        }
    }]

    // handle validation
    const handleValidation = (formData) => {
        let invalidCount = 0;

        const titleValidation = validationService.minLength(formData.title, 3)
        setError('title', { message: titleValidation.message })
        if (!titleValidation.isValid) {
            invalidCount++;
        }

        const descriptionValidation = validationService.minLength(formData.description, 3)
        setError('description', { message: descriptionValidation.message })
        if (!descriptionValidation.isValid) {
            invalidCount++;
        }

        return invalidCount !== 0;
    }

    // handle form submission
    const onSubmit = async (formData) => {
        const isInvalid = handleValidation(formData)

        if (isInvalid) {
            return;
        }

        try {
            let response;
            if (formData.id) {
                // make an api call and update redux store to handle redirection
                response = await todoApiService.update({
                    id: formData.id,
                    title: formData.title,
                    description: formData.description,
                })
            } else {
                // make an api call and update redux store to handle redirection
                response = await todoApiService.create({
                    title: formData.title,
                    description: formData.description,
                })
            }

            // if user is success then update redux store
            if (response.data) {
                // fetch todos and update list
                const response = await todoApiService.get();
                console.log({ response })
            }
        } catch (error) {
            alert("Create todo failed")
            console.log("api faield", error)
        }
    }

    return (
        <Container>
            <h3>TODOs</h3>


            <Form onSubmit={handleSubmit(onSubmit)}>
                <Stack direction="horizontal" gap={3} className="align-items-start">
                    <Controller
                        name="title"
                        control={control}
                        render={({ field, fieldState }) => (
                            <FormGroup as={Stack} direction="horizontal" gap={2} className="mb-4 align-items-start">
                                <FormLabel>Title*</FormLabel>
                                <Stack>
                                    <FormControl
                                        placeholder="Please enter title here"
                                        {...field}
                                    />
                                    {fieldState?.error?.message ? (
                                        <FormText className="text-danger">{fieldState.error.message}</FormText>
                                    ) : null}
                                </Stack>
                            </FormGroup>
                        )}
                    />

                    <Controller
                        name="description"
                        control={control}
                        render={({ field, fieldState }) => (
                            <FormGroup as={Stack} direction="horizontal" gap={2} className="mb-4 align-items-start">
                                <FormLabel>Description*</FormLabel>
                                <Stack>
                                    <FormControl
                                        placeholder="Please enter description here"
                                        {...field}
                                    />

                                    {fieldState?.error?.message ? (
                                        <FormText className="text-danger">{fieldState.error.message}</FormText>
                                    ) : null}
                                </Stack>
                            </FormGroup>
                        )}
                    />

                    <Button type="submit" variant="success">Save</Button>
                </Stack>
            </Form>

            <Table>
                <thead>
                    <tr>
                        {columns.map((col) => <th key={col.label}>{col.label}</th>)}
                    </tr>
                </thead>
                <tbody>
                    {todos.map((todo, todoIndex) => (
                        <tr key={`todo_${todoIndex}`}>
                            {columns.map((col, colIndex) => <td key={`todo_${todoIndex}_col_${colIndex}`}>{col.renderer(todo, todoIndex, col, colIndex)}</td>)}
                        </tr>
                    ))}
                </tbody>
            </Table>
        </Container>
    )
}

export default Dashboard