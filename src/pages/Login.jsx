import { Controller, useForm } from "react-hook-form"
import { useDispatch } from "react-redux"

import { Button, Card, CardBody, Col, Container, Form, FormControl, FormGroup, FormLabel, Row } from "react-bootstrap"
import { validationService } from "../shared/services/validationService"
import { userApiService } from "../shared/services/userApiService"
import { userActions } from "../store/slices/userSlice"

const Login = () => {
    const dispatch = useDispatch();
    // handle form setting
    const {
        control,
        handleSubmit,
        setError,
    } = useForm({
        defaultValues: {
            username: '',
            password: '',
        }
    })

    // handle validation
    const handleValidation = (formData) => {
        let invalidCount = 0;

        const usernameValidation = validationService.required(formData.username)
        setError('username', { message: usernameValidation.message })
        if (!usernameValidation.isValid) {
            invalidCount++;
        }

        const passwordValidation = validationService.required(formData.password)
        setError('password', { message: passwordValidation.message })
        if (!passwordValidation.isValid) {
            invalidCount++;
        }

        return invalidCount !== 0;
    }

    // handle form submission
    const onSubmit = async (formData) => {
        const isInvalid = handleValidation(formData)
        console.log({ isInvalid })
        if (isInvalid) {
            return;
        }

        try {
            // make an api call and update redux store to handle redirection
            const response = await userApiService.login({
                username: formData.username,
                password: formData.password
            })

            // if user is success then update redux store
            if (response.data) {
                dispatch(userActions.setAuthenticated({ token: response.data.token }))
            }
        } catch (error) {
            alert("Api call failed")
            console.log("api faield", error)
            dispatch(userActions.setAuthenticated({ token: 'tempToken' }))
        }
    }

    return (
        <Container fluid>
            <Row className="justify-content-center align-items-center min-vh-100">
                <Col xs={11} sm={4}>
                    <p className="text-center">Todo App</p>
                    <Card>
                        <CardBody className="p-5">
                            <Form onSubmit={handleSubmit(onSubmit)}>
                                <Controller
                                    name="username"
                                    control={control}
                                    render={({ field, fieldState }) => (
                                        <FormGroup className="mb-4">
                                            <FormLabel>Username*</FormLabel>
                                            <FormControl
                                                placeholder="Please enter username here"
                                                {...field}
                                            />
                                            {fieldState?.error?.message ? (
                                                <Form.Text className="text-danger">{fieldState.error.message}</Form.Text>
                                            ) : null}
                                        </FormGroup>
                                    )}
                                />

                                <Controller
                                    name="password"
                                    control={control}
                                    render={({ field, fieldState }) => (
                                        <FormGroup className="mb-4">
                                            <FormLabel>Password*</FormLabel>
                                            <FormControl
                                                type="password"
                                                placeholder="Please enter password here"
                                                {...field}
                                            />
                                            {fieldState?.error?.message ? (
                                                <Form.Text className="text-danger">{fieldState.error.message}</Form.Text>
                                            ) : null}
                                        </FormGroup>
                                    )}
                                />

                                <Button className="w-100" type="submit" variant="success">Login</Button>
                            </Form>

                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

export default Login