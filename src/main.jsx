import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter as Router } from 'react-router-dom'
import { Provider as ReduxProvider } from 'react-redux'

import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App.jsx'
import store, { persistor } from './store';
import { PersistGate } from 'redux-persist/integration/react';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ReduxProvider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router basename='/'>
          <App />
        </Router>
      </PersistGate>
    </ReduxProvider>
  </React.StrictMode>,
)
