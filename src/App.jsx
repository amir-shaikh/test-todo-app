import { useSelector } from "react-redux"
import { Routes, Route } from "react-router-dom"

import PublicRoute from "./shared/routes/PublicRoute"
import PrivateRoute from "./shared/routes/PrivateRoute"

import Login from "./pages/Login"
import Dashboard from "./pages/Dashboard"

const App = () => {
  const isAuthenticated = useSelector(state => state.user.isAuthenticated)
  
  return (
    <>
      <Routes>
        {/* Routes which should be allowed to be used publicly and restricted by authenticated users */}
        <Route element={<PublicRoute isAuthenticated={isAuthenticated} />}>
          <Route path="/login" element={<Login />} />
        </Route>

        {/* Routes which should be allowed to be used authenticated users and restricted by public users */}
        <Route element={<PrivateRoute isAuthenticated={isAuthenticated} /> }>
          <Route path="/" element={<Dashboard />} />
        </Route>
      </Routes>
    </>
  )
}

export default App
